package com.abc;

import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.authority.AuthorityRule;
import com.alibaba.csp.sentinel.slots.block.authority.AuthorityRuleManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class AuthSentinelConsumer8080 {

    public static void main(String[] args) {
        SpringApplication.run(AuthSentinelConsumer8080.class, args);
        initRule();
    }

    private static void initRule() {
        List<AuthorityRule> list = new ArrayList<>();
        AuthorityRule rule = AuthSentinelConsumer8080.configRule();
        list.add(rule);
        AuthorityRuleManager.loadRules(list);
    }

    private static AuthorityRule configRule() {
        AuthorityRule rule = new AuthorityRule();
        rule.setResource("getHandle");
        rule.setStrategy(RuleConstant.AUTHORITY_WHITE);
        rule.setLimitApp("sa,sb,sc");
        return rule;
    }

}
