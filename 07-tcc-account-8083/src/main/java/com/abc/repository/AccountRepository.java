package com.abc.repository;

import com.abc.bean.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Integer> {
    Account findAccountByuserId(Integer userId);
}
