package com.abc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TccAccount8083 {

    public static void main(String[] args) {
        SpringApplication.run(TccAccount8083.class, args);
    }

}
