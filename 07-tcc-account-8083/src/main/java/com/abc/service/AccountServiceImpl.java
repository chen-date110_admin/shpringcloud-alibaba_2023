package com.abc.service;

import com.abc.bean.Account;
import com.abc.repository.AccountRepository;
import io.seata.rm.tcc.api.BusinessActionContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Slf4j
@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository repository;

    @Override
    public Account debitAccount(Integer userId, Double amount) {
        Account account = repository.findAccountByuserId(userId);
        if (account == null || account.getBalance() < amount) {
            return null;
        }
        account.setBalance(account.getBalance() - amount);
        return repository.save(account);
    }

    @Override
    public Boolean tccCommit(BusinessActionContext context) {
        log.info("账户表变更已确认提交");
        return true;
    }

    @Override
    public Boolean tccRollback(BusinessActionContext context) {
        log.info("账户表变更已发生回滚");

        // 从上下文中获取指定的参数
        Integer userId = (Integer) context.getActionContext("userId");
        // 对于Double数据，其会被转换为BigDecimal类型写入到context中
        BigDecimal amount = (BigDecimal) context.getActionContext("amount");

        Account account = repository.findAccountByuserId(userId);
        account.setBalance(account.getBalance() + amount.doubleValue());
        repository.save(account);

        return true;
    }
}
