package com.abc.service;

import com.abc.bean.Account;
import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.BusinessActionContextParameter;
import io.seata.rm.tcc.api.LocalTCC;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;

@LocalTCC
public interface AccountService {

    @TwoPhaseBusinessAction(name = "debitAccount",
            commitMethod = "tccCommit",
            rollbackMethod = "tccRollback")
    Account debitAccount(@BusinessActionContextParameter("userId") Integer userId,
                         @BusinessActionContextParameter("amount") Double amount);

    // 手动提交方法
    Boolean tccCommit(BusinessActionContext context);

    // 手动回滚方法
    Boolean tccRollback(BusinessActionContext context);
}
