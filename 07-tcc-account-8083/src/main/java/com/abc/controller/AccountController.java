package com.abc.controller;

import com.abc.bean.Account;
import com.abc.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {

    @Autowired
    private AccountService service;

    @PutMapping("/account/debit")
    public Account debitHandle(@RequestParam("userId") Integer userId,
                                @RequestParam("amount") Double amount) {
        return service.debitAccount(userId, amount);
    }

}
