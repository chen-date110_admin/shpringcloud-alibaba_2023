package com.abc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Stock8081 {

    public static void main(String[] args) {
        SpringApplication.run(Stock8081.class, args);
    }

}
