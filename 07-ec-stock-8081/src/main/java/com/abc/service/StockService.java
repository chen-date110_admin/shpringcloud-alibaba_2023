package com.abc.service;

import com.abc.bean.Stock;

public interface StockService {
    Stock deductStock(Integer goodsId, Integer count);
}
