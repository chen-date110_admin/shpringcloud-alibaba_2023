package com.abc.service;

import com.abc.bean.Stock;
import com.abc.repository.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StockServiceImpl implements StockService {

    @Autowired
    private StockRepository repository;

    @Override
    public Stock deductStock(Integer goodsId, Integer count) {
        Stock stock = repository.findStockByGoodsId(goodsId);
        if (stock == null || stock.getTotal() < count) {
            return null;  // 若库存不足，则返回null
        }
        stock.setTotal(stock.getTotal() - count);
        return repository.save(stock);
    }
}
