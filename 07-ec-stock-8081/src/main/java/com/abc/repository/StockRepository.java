package com.abc.repository;

import com.abc.bean.Stock;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StockRepository extends JpaRepository<Stock, Integer> {

    Stock findStockByGoodsId(Integer goodsId);
}
