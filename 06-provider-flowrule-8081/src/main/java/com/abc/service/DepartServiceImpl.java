package com.abc.service;

import com.abc.bean.Depart;
import com.abc.repository.DepartRepository;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DepartServiceImpl implements DepartService{

    @Autowired
    private DepartRepository repository;

    @Value("${server.port}")
    private int port;

    @Override
    public boolean saveDepart(Depart depart) {
        Depart obj = repository.save(depart);
        if (obj != null) {
            return true;
        }
        return false;
    }

    @Override
    public boolean removeDepartById(int id) {
        if (repository.existsById(id)) {
            repository.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public boolean modifyDepart(Depart depart) {
        Depart obj = repository.save(depart);
        if (obj != null) {
            return true;
        }
        return false;
    }

    @Override
    public Depart getDepartById(int id) {
        if (repository.existsById(id)) {
            return repository.getReferenceById(id);
        }
        Depart depart = new Depart();
        depart.setName("no this depart：" + port);
        return depart;
    }

    @SentinelResource(value = "listAllDeparts", blockHandler = "listHandleFlowFallback")
    @Override
    public List<Depart> listAllDeparts() {
        return repository.findAll();
    }

    public List<Depart> listHandleFlowFallback(BlockException e) {
        List<Depart> list = new ArrayList<>();
        Depart depart = new Depart();
        depart.setName("list-flow-sentinel-method");
        list.add(depart);
        return list;
    }
}
