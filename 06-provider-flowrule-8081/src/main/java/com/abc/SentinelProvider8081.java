package com.abc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SentinelProvider8081 {

    public static void main(String[] args) {
        SpringApplication.run(SentinelProvider8081.class, args);
    }
}
