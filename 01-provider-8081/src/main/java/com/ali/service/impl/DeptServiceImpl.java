package com.ali.service.impl;

import com.ali.admin.Dept;
import com.ali.mapper.DeptMapper;
import com.ali.service.DeptService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeptServiceImpl implements DeptService {
//    @Autowired
    @Resource
    private DeptMapper deptMapper;

    @Override
    public boolean saveDept(Dept dept) {
        Dept save = deptMapper.save(dept);
        if (save != null) {
            return true;
        }
        return false;
    }

    @Override
    public boolean removeDeptById(int id) {
        if (deptMapper.existsById(id)) {
            deptMapper.deleteById(id);
            return true;
        }
        return false;
    }


    @Override
    public boolean updateDept(Dept dept) {
        Dept save = deptMapper.save(dept);
        if (save != null) {
            return true;
        }
        return false;
    }

    @Override
    public Dept getDeptById(int id) {
        if (deptMapper.existsById(id)) {
           return deptMapper.getReferenceById(id);

        }
        Dept dept = new Dept();
        dept.setName("no this dept");
        return dept;
    }

    @Override
    public List<Dept> getAllDept() {
        return deptMapper.findAll();
    }
}
