package com.ali.service;

import com.ali.admin.Dept;

import java.util.List;

public interface DeptService {
   //新增
    boolean saveDept(Dept dept);
    //删除
    boolean removeDeptById(int id);
    //修改
    boolean updateDept(Dept dept);
    //根据id查询
    Dept getDeptById(int id);
    //查询所有
    List<Dept> getAllDept();
}
