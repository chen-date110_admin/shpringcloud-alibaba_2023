package com.ali.admin;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

@Data
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer","handler","fieldHandler"})//忽略延迟加载
public class Dept {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //自增
    private Integer id;
    private String name;
}
