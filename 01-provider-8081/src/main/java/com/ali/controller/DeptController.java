package com.ali.controller;

import com.ali.admin.Dept;
import com.ali.service.DeptService;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/dept")
public class DeptController {
    @Resource
    private DeptService deptService;

    @PostMapping("/save")
    public void saveHandle(@RequestBody String  dept) {
        System.out.println(dept);
//        return deptService.saveDept(dept);
    }

    @DeleteMapping("/delete/{id}")
    public boolean deleteHandle(@PathVariable int id) {
        return deptService.removeDeptById(id);

    }

    @PutMapping("/update")
    public boolean updateHandle(@RequestBody Dept dept) {
        return deptService.updateDept(dept);
    }

    @GetMapping("/get/{id}")
    public Dept getHandle(@PathVariable int id) {
        return deptService.getDeptById(id);
    }

    @GetMapping("/list")
    public List<Dept> getAllHandle() {
        return deptService.getAllDept();
    }
}
