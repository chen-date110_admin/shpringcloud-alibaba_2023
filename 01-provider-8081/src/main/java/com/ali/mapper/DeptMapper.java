package com.ali.mapper;

import com.ali.admin.Dept;
import org.springframework.data.jpa.repository.JpaRepository;
public interface DeptMapper extends JpaRepository<Dept,Integer> {
}
