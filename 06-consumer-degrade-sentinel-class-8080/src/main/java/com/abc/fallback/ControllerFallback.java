package com.abc.fallback;

import com.abc.bean.Depart;

import java.util.ArrayList;
import java.util.List;

public class ControllerFallback {
    // 定义getHandle()方法的服务降级方法
    public static Depart getHandleFallback(int id, Throwable t) {
        Depart depart = new Depart();
        depart.setId(id);
        depart.setName("degrade-sentinel-class-" + t.getMessage());
        return depart;
    }

    // 定义listHandle()方法的服务降级方法
    public static List<Depart> listHandleFallback(Throwable t) {
        List<Depart> list = new ArrayList<>();
        Depart depart = new Depart();
        depart.setName("list-degrade-sentinel-class-" + t.getMessage());
        list.add(depart);
        return list;
    }
}
