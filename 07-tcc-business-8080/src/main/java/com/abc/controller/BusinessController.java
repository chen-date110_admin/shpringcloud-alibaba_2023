package com.abc.controller;

import com.abc.bean.Goodsorder;
import com.abc.bean.Stock;
import com.abc.service.GoodsorderService;
import com.abc.service.StockService;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BusinessController {

    @Autowired
    private GoodsorderService goodsorderService;

    @Autowired
    private StockService stockService;

    @GlobalTransactional  // 当前处理方法就变为了TM
    @GetMapping("/business/purchase")
    public String purchaseHandle(@RequestParam("userId") Integer userId,
                                     @RequestParam("goodsId") Integer goodsId,
                                     @RequestParam("goodsPrice") Double goodsPrice,
                                     @RequestParam("count") Integer count) {
        // 扣减库存
        Stock stock = stockService.deductStock(goodsId, count);
        if (stock == null) {
            return "库存不足，下单失败";
        }

        Goodsorder goodsorder = new Goodsorder();
        goodsorder.setUserId(userId);
        goodsorder.setGoodsId(goodsId);
        goodsorder.setGoodsPrice(goodsPrice);
        goodsorder.setCount(count);

        // 生成订单
        Goodsorder order = goodsorderService.createGoodsorder(goodsorder);
        if (order == null) {
            return "账户余额不足，下单失败";
        }

        // 抛出异常
        // int i = 3/0;

        return "下单成功";
    }

}
