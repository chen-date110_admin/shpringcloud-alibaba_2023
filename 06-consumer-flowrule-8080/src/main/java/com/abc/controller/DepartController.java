package com.abc.controller;

import com.abc.bean.Depart;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/consumer/depart")
@RestController
public class DepartController {

    @Autowired
    private RestTemplate template;

    // 直连方式
    // private static final String SERVICE_PROCIER = "http://localhost:8081/provider/depart";

    // 微服务方式
    private static final String SERVICE_PROCIER = "http://depart-provider/provider/depart";

    @PostMapping("/save")
    public boolean saveHandle(@RequestBody Depart depart) {
        String url = SERVICE_PROCIER + "/save";
        return template.postForObject(url, depart, Boolean.class);
    }

    @DeleteMapping("/del/{id}")
    public void deleteHandle(@PathVariable("id") int id) {
        template.delete(SERVICE_PROCIER + "/del/" + id);
    }

    @PutMapping("/update")
    public void updateHandle(@RequestBody Depart depart) {
        String url = SERVICE_PROCIER + "/update";
        template.put(url, depart);
    }

    @SentinelResource(value = "getHandle", fallback = "getHandleFallback", blockHandler = "getHandleFlowFallback")
    @GetMapping("/get/{id}")
    public Depart getHandle(@PathVariable("id") int id) {
        String url = SERVICE_PROCIER + "/get/" + id;
        return template.getForObject(url, Depart.class);
    }

    // @SentinelResource(value = "getHandle", fallback = "getHandleFallback", blockHandler = "getHandleFlowFallback")
    // @GetMapping("/get/{id}")
    // public Depart getHandle(@PathVariable("id") int id) {
    //     Entry entry = null;
    //     try {
    //         entry = SphU.entry("listHandle");
    //         String url = SERVICE_PROCIER + "/get/" + id;
    //         return template.getForObject(url, Depart.class);
    //     } catch (BlockException e) {
    //         Depart depart = new Depart();
    //         depart.setId(id);
    //         depart.setName("entry-flow-sentinel-method-" + e.getMessage());
    //         return depart;
    //     } finally {
    //         if (entry != null) {
    //             entry.exit();
    //         }
    //     }
    // }

    // 定义getHandle()方法的服务流控方法
    public Depart getHandleFlowFallback(int id, BlockException be) {
        Depart depart = new Depart();
        depart.setId(id);
        depart.setName("flow-sentinel-method-" + be.getMessage());
        return depart;
    }

    // 定义getHandle()方法的服务降级方法
    public Depart getHandleFallback(int id, Throwable t) {
        Depart depart = new Depart();
        depart.setId(id);
        depart.setName("degrade-sentinel-method-" + t.getMessage());
        return depart;
    }

    @SentinelResource(value = "listHandle", blockHandler = "listHandleFlowFallback")
    @GetMapping("/list")
    public List<Depart> listHandle() {
        String url = SERVICE_PROCIER + "/list";
        return template.getForObject(url, List.class);
    }

    public List<Depart> listHandleFlowFallback(BlockException e) {
        List<Depart> list = new ArrayList<>();
        Depart depart = new Depart();
        depart.setName("list-flow-sentinel-method");
        list.add(depart);
        return list;
    }
}
