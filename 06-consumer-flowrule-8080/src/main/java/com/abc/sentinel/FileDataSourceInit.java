package com.abc.sentinel;

import com.alibaba.csp.sentinel.command.handler.ModifyParamFlowRulesCommandHandler;
import com.alibaba.csp.sentinel.datasource.FileRefreshableDataSource;
import com.alibaba.csp.sentinel.datasource.FileWritableDataSource;
import com.alibaba.csp.sentinel.datasource.ReadableDataSource;
import com.alibaba.csp.sentinel.datasource.WritableDataSource;
import com.alibaba.csp.sentinel.init.InitFunc;
import com.alibaba.csp.sentinel.slots.block.authority.AuthorityRule;
import com.alibaba.csp.sentinel.slots.block.authority.AuthorityRuleManager;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRule;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRuleManager;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRuleManager;
import com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowRuleManager;
import com.alibaba.csp.sentinel.slots.system.SystemRule;
import com.alibaba.csp.sentinel.slots.system.SystemRuleManager;
import com.alibaba.csp.sentinel.transport.util.WritableDataSourceRegistry;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileDataSourceInit implements InitFunc {

    @Override
    public void init() throws Exception {
        // 在当前工程根目录下创建/rules目录
        File ruleFileDir = new File(System.getProperty("user.dir") + "/rules");
        // 创建目录
        if (!ruleFileDir.exists()) {
            ruleFileDir.mkdirs();
        }

        readWriteRuleFileFlow(ruleFileDir.getPath());
        readWriteRuleFileDegrade(ruleFileDir.getPath());
        readWriteRuleFileAuthority(ruleFileDir.getPath());
        readWriteRuleFileSystem(ruleFileDir.getPath());
        readWriteRuleFileParamFlow(ruleFileDir.getPath());
    }

    // 针对FlowRule
    private void readWriteRuleFileFlow(String ruleFilePath) throws IOException {
        String ruleFile = ruleFilePath + "/flow-rule.json";
        createRuleFile(ruleFile);
        ReadableDataSource<String, List<FlowRule>> ds = new FileRefreshableDataSource<>(
                ruleFile, source -> JSON.parseObject(source, new TypeReference<List<FlowRule>>() {})
        );
        // 将可读数据源注册至 FlowRuleManager.
        FlowRuleManager.register2Property(ds.getProperty());
        WritableDataSource<List<FlowRule>> wds = new FileWritableDataSource<>(ruleFile, this::encodeJson);
        // 将可写数据源注册至 transport 模块的 WritableDataSourceRegistry 中.
        // 这样收到控制台推送的规则时，Sentinel 会先更新到内存，然后将规则写入到文件中.
        WritableDataSourceRegistry.registerFlowDataSource(wds);
    }
    // 针对DegradeRule
    private void readWriteRuleFileDegrade(String ruleFilePath) throws IOException {
        String ruleFile = ruleFilePath + "/degrade-rule.json";
        createRuleFile(ruleFile);
        ReadableDataSource<String, List<DegradeRule>> ds = new FileRefreshableDataSource<>(
                ruleFile, source -> JSON.parseObject(source, new TypeReference<List<DegradeRule>>() {})
        );
        DegradeRuleManager.register2Property(ds.getProperty());
        WritableDataSource<List<DegradeRule>> wds = new FileWritableDataSource<>(ruleFile, this::encodeJson);
        WritableDataSourceRegistry.registerDegradeDataSource(wds);
    }

    // 针对AuthorityRule
    private void readWriteRuleFileAuthority(String ruleFilePath) throws IOException {
        String ruleFile = ruleFilePath + "/authority-rule.json";
        createRuleFile(ruleFile);

        ReadableDataSource<String, List<AuthorityRule>> ds = new FileRefreshableDataSource<>(
                ruleFile, source -> JSON.parseObject(source, new TypeReference<List<AuthorityRule>>() {})
        );
        AuthorityRuleManager.register2Property(ds.getProperty());

        WritableDataSource<List<AuthorityRule>> wds = new FileWritableDataSource<>(ruleFile, this::encodeJson);
        WritableDataSourceRegistry.registerAuthorityDataSource(wds);
    }
    // 针对SystemRule
    private void readWriteRuleFileSystem(String ruleFilePath) throws IOException {
        String ruleFile = ruleFilePath + "/system-rule.json";
        createRuleFile(ruleFile);

        ReadableDataSource<String, List<SystemRule>> ds = new FileRefreshableDataSource<>(
                ruleFile, source -> JSON.parseObject(source, new TypeReference<List<SystemRule>>() {})
        );
        SystemRuleManager.register2Property(ds.getProperty());

        WritableDataSource<List<SystemRule>> wds = new FileWritableDataSource<>(ruleFile, this::encodeJson);
        WritableDataSourceRegistry.registerSystemDataSource(wds);
    }
    // 针对ParamFlowRule
    private void readWriteRuleFileParamFlow(String ruleFilePath) throws IOException {
        String ruleFile = ruleFilePath + "/param-rule.json";
        createRuleFile(ruleFile);

        ReadableDataSource<String, List<ParamFlowRule>> ds = new FileRefreshableDataSource<>(
                ruleFile, source -> JSON.parseObject(source, new TypeReference<List<ParamFlowRule>>() {})
        );
        ParamFlowRuleManager.register2Property(ds.getProperty());

        WritableDataSource<List<ParamFlowRule>> wds = new FileWritableDataSource<>(ruleFile, this::encodeJson);
        // 注意，这里与其它的是不同的
        ModifyParamFlowRulesCommandHandler.setWritableDataSource(wds);
    }

    private void createRuleFile(String ruleFile) throws IOException {

        File file = new File(ruleFile);
        // 创建文件
        if (!file.exists()) {
            file.createNewFile();
        }
    }

    private <T> String encodeJson(T t) {
        // 将对象t转换为JSON串
        return JSON.toJSONString(t);
    }



}
