package com.abc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TccStock8081 {

    public static void main(String[] args) {
        SpringApplication.run(TccStock8081.class, args);
    }

}
