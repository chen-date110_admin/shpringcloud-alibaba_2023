package com.abc.controller;

import com.abc.bean.Stock;
import com.abc.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StockController {

    @Autowired
    private StockService service;

    @PutMapping("/stock/deduct")
    public Stock deductHandle(@RequestParam("goodsId") Integer goodsId,
                              @RequestParam("count") Integer count) {
        return service.deductStock(goodsId, count);
    }

}
