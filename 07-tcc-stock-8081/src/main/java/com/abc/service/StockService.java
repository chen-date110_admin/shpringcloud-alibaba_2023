package com.abc.service;

import com.abc.bean.Stock;
import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.BusinessActionContextParameter;
import io.seata.rm.tcc.api.LocalTCC;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;

@LocalTCC
public interface StockService {

    @TwoPhaseBusinessAction(name = "deductStock",
                            commitMethod = "tccCommit",
                            rollbackMethod = "tccRollback")
    Stock deductStock(@BusinessActionContextParameter("goodsId") Integer goodsId,
                      @BusinessActionContextParameter("count") Integer count);

    // 手动提交方法
    Boolean tccCommit(BusinessActionContext context);

    // 手动回滚方法
    Boolean tccRollback(BusinessActionContext context);
}
