package com.abc.service;

import com.abc.bean.Stock;
import com.abc.repository.StockRepository;
import io.seata.rm.tcc.api.BusinessActionContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class StockServiceImpl implements StockService {

    @Autowired
    private StockRepository repository;

    @Override
    public Stock deductStock(Integer goodsId, Integer count) {
        Stock stock = repository.findStockByGoodsId(goodsId);
        if (stock == null || stock.getTotal() < count) {
            return null;  // 若库存不足，则返回null
        }
        stock.setTotal(stock.getTotal() - count);
        return repository.save(stock);
    }

    @Override
    public Boolean tccCommit(BusinessActionContext context) {
        log.info("库存表变更已确认提交");
        return true;
    }

    @Override
    public Boolean tccRollback(BusinessActionContext context) {
        log.info("库存表变更已发生回滚");

        // 从上下文中获取指定的参数
        Integer goodsId = (Integer) context.getActionContext("goodsId");
        Integer count = (Integer) context.getActionContext("count");

        Stock stock = repository.findStockByGoodsId(goodsId);
        stock.setTotal(stock.getTotal() + count);
        repository.save(stock);

        return true;
    }
}
