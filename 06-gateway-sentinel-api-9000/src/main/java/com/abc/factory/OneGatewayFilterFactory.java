package com.abc.factory;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractNameValueGatewayFilterFactory;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class OneGatewayFilterFactory extends AbstractNameValueGatewayFilterFactory {
    @Override
    public GatewayFilter apply(NameValueConfig config) {
        return (exchange, chain) -> {
           // pre-filter
            long start = System.currentTimeMillis();
            log.info(config.getName() + "-" + config.getValue() + "-pre，开始执行的时间：" + start);
            exchange.getAttributes().put("startTime", start);
            return chain.filter(exchange).then(
                   // post-filter
                    Mono.fromRunnable(() -> {
                        long startTime = (long) exchange.getAttributes().get("startTime");
                        long endTime = System.currentTimeMillis();
                        long elapsedTime = endTime - startTime;
                        log.info(config.getName() + "-" + config.getValue() + "-post，执行完毕的时间：" + endTime);
                        log.info("该filter执行用时(毫秒)：" + elapsedTime);
                    })
           );
        };
    }
}
