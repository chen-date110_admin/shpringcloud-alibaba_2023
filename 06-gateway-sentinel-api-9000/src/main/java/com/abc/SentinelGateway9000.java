package com.abc;

import com.alibaba.csp.sentinel.adapter.gateway.common.SentinelGatewayConstants;
import com.alibaba.csp.sentinel.adapter.gateway.common.api.ApiDefinition;
import com.alibaba.csp.sentinel.adapter.gateway.common.api.ApiPathPredicateItem;
import com.alibaba.csp.sentinel.adapter.gateway.common.api.ApiPredicateItem;
import com.alibaba.csp.sentinel.adapter.gateway.common.api.GatewayApiDefinitionManager;
import com.alibaba.csp.sentinel.adapter.gateway.common.rule.GatewayFlowRule;
import com.alibaba.csp.sentinel.adapter.gateway.common.rule.GatewayRuleManager;
import com.alibaba.csp.sentinel.adapter.gateway.sc.callback.GatewayCallbackManager;
import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerResponse;

import java.util.*;

@SpringBootApplication
public class SentinelGateway9000 {

    public static void main(String[] args) {
        SpringApplication.run(SentinelGateway9000.class, args);
        initBlockHandler();
        initGatewayRule();
        initCustomizedApis();
    }

    private static void initCustomizedApis() {
        ApiDefinition api1 = new ApiDefinition("some_customized_api")
                .setPredicateItems(new HashSet<ApiPredicateItem>() {{
                    add(new ApiPathPredicateItem().setPattern("/consumer/depart/save"));
                    add(new ApiPathPredicateItem().setPattern("/consumer/depart/update")
                            .setMatchStrategy(SentinelGatewayConstants.URL_MATCH_STRATEGY_EXACT));
                }});

        ApiDefinition api2 = new ApiDefinition("another_customized_api")
                .setPredicateItems(new HashSet<ApiPredicateItem>() {{
                    add(new ApiPathPredicateItem().setPattern("/consumer/depart/get/**")
                            .setMatchStrategy(SentinelGatewayConstants.URL_MATCH_STRATEGY_PREFIX));
                }});


        Set<ApiDefinition> definitions = new HashSet<ApiDefinition>() {{
            add(api1);
            add(api2);
        }};
        GatewayApiDefinitionManager.loadApiDefinitions(definitions);
    }

    private static void initGatewayRule() {
        Set<GatewayFlowRule> rules = new HashSet<>();
        GatewayFlowRule rule = SentinelGateway9000.configGatewayFlowRule();
        rules.add(rule);
        GatewayRuleManager.loadRules(rules);
    }

    private static GatewayFlowRule configGatewayFlowRule() {
        GatewayFlowRule rule = new GatewayFlowRule();
        // 指定资源模式为API
        rule.setResourceMode(SentinelGatewayConstants.RESOURCE_MODE_CUSTOM_API_NAME);
        // 指定要限流的自定义API分组
        rule.setResource("another_customized_api");
        // 指定限流规则为QPS流控，默认值
        rule.setGrade(RuleConstant.FLOW_GRADE_QPS);
        // 指定QPS阈值
        rule.setCount(3);
        // 指定统计时长，默认值为1秒
        rule.setIntervalSec(1);
        // 指定流控方式为“快速失败”，默认值
        // rule.setControlBehavior(RuleConstant.CONTROL_BEHAVIOR_DEFAULT);
        // 指定流控方式为“匀速排队"
        rule.setControlBehavior(RuleConstant.CONTROL_BEHAVIOR_RATE_LIMITER);
        // 指定排队等待的超时时间，默认500毫秒
        rule.setMaxQueueingTimeoutMs(500);
        return rule;
    }

    private static void initBlockHandler() {
        // 重定向
        // GatewayCallbackManager.setBlockHandler(new RedirectBlockRequestHandler("https://baidu.com"));

        // 自定义异常结果
        GatewayCallbackManager.setBlockHandler((exchange,th) -> {
            Map<String, Object> map = new HashMap<>();
            map.put("uri", exchange.getRequest().getURI());
            map.put("msg", "访问量过大，稍候请重试");
            map.put("status", 429);
            return ServerResponse.status(HttpStatus.TOO_MANY_REQUESTS)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(BodyInserters.fromValue(map));
        });

    }

}
