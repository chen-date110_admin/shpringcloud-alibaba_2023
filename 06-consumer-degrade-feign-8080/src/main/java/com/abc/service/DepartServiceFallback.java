package com.abc.service;

import com.abc.bean.Depart;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Component
@RequestMapping("/fallback/provider/depart")
public class DepartServiceFallback implements DepartService {
    @Override
    public boolean saveDepart(Depart depart) {
        System.out.println("执行 saveDepart() 降级");
        return false;
    }

    @Override
    public boolean removeDepartById(int id) {
        System.out.println("执行 removeDepartById() 降级");
        return false;
    }

    @Override
    public boolean modifyDepart(Depart depart) {
        System.out.println("执行 modifyDepart() 降级");
        return false;
    }

    @Override
    public Depart getDepartById(int id) {
        System.out.println("执行 getDepartById() 降级");
        Depart depart = new Depart();
        depart.setId(id);
        depart.setName("degrade-feign");
        return depart;
    }

    @Override
    public List<Depart> listAllDeparts() {
        System.out.println("执行 listAllDeparts() 降级");
        List<Depart> list = new ArrayList<>();
        Depart depart = new Depart();
        depart.setName("list-degrade-feign");
        list.add(depart);
        return list;
    }
}
