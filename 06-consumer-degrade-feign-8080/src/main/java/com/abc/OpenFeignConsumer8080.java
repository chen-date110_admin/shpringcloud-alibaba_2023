package com.abc;

import com.abc.config.DepartConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClients;
import org.springframework.cloud.openfeign.EnableFeignClients;

@LoadBalancerClients(defaultConfiguration = DepartConfig.class)
@SpringBootApplication
@EnableFeignClients   // 开启OpenFeign
public class OpenFeignConsumer8080 {

    public static void main(String[] args) {
        SpringApplication.run(OpenFeignConsumer8080.class, args);
    }

}
