package com.abc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.context.annotation.Bean;
import reactor.core.publisher.Mono;

import java.util.Objects;

@SpringBootApplication
public class ConigGateway9000 {

    public static void main(String[] args) {
        SpringApplication.run(ConigGateway9000.class, args);
    }

    // @Bean
    // public RouteLocator filterCircuitBreakerRouteLocator(RouteLocatorBuilder builder) {
    //     return builder.routes()
    //             .route("filterCircuitBreaker_route",
    //                     ps -> ps.path("/info/**")
    //                             .filters(fs -> fs.circuitBreaker(config -> {
    //                                 config.setName("myCircuitBreaker");
    //                                 config.setFallbackUri("forward:/fb3");
    //                             }))
    //                             .uri("https://localhost:8080"))
    //             .build();
    // }

    // @Bean
    // KeyResolver userKeyResolver() {
    //     return exchange -> Mono.just(exchange.getRequest().getQueryParams().getFirst("user"));
    // }

    @Bean
    KeyResolver hostKeyResolver() {
        return exchange -> Mono.just(Objects.requireNonNull(exchange.getRequest().getRemoteAddress()).getHostName());
    }

    // @Bean
    // KeyResolver pathKeyResolver() {
    //     return exchange -> Mono.just(exchange.getRequest().getPath().value());
    // }
}
