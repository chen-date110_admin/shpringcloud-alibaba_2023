package com.abc.factory;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractNameValueGatewayFilterFactory;
import org.springframework.cloud.gateway.support.ServerWebExchangeUtils;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import java.util.Map;

@Component
public class AddHeaderGatewayFilterFactory extends AbstractNameValueGatewayFilterFactory {
    @Override
    public GatewayFilter apply(NameValueConfig config) {
        return (exchange, chain) -> {
            Map<String, String> uriVariables = ServerWebExchangeUtils.getUriTemplateVariables(exchange);

            String bean = uriVariables.get("bean");
            String id = uriVariables.get("id");

            System.out.println("bean-id   " + bean + "-" + id);

            ServerHttpRequest changedRequest = exchange.getRequest()
                                                       .mutate()
                                                       .header(config.getName(), config.getValue())
                                                       .build();

            ServerWebExchange changedExchange = exchange.mutate()
                                                        .request(changedRequest)
                                                        .build();
            return chain.filter(changedExchange);
        };
    }
}
