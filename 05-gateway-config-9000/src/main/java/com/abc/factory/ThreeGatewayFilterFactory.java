package com.abc.factory;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractNameValueGatewayFilterFactory;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class ThreeGatewayFilterFactory extends AbstractNameValueGatewayFilterFactory {
    @Override
    public GatewayFilter apply(NameValueConfig config) {
        return (exchange, chain) -> {
           // pre-filter
            log.info(config.getName() + "-" + config.getValue() + "-pre");
            return chain.filter(exchange).then(
                   // post-filter
                    Mono.fromRunnable(() -> {
                        log.info(config.getName() + "-" + config.getValue() + "-post");
                    })
           );
        };
    }
}
