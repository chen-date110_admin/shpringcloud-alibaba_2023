package com.abc.factory;

import lombok.Data;
import org.springframework.cloud.gateway.handler.predicate.AbstractRoutePredicateFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.server.ServerWebExchange;

import java.net.InetSocketAddress;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

@Component
public class TokenRoutePredicateFactory extends AbstractRoutePredicateFactory<TokenRoutePredicateFactory.Config> {

    public TokenRoutePredicateFactory() {
        super(Config.class);
    }

    @Override
    public Predicate<ServerWebExchange> apply(Config config) {
        return exchange -> {

            InetSocketAddress remoteAddress = exchange.getRequest().getRemoteAddress();
            InetSocketAddress localAddress = exchange.getRequest().getLocalAddress();

            System.out.println(remoteAddress);
            System.out.println(localAddress);

            // 获取请求中的所有请求参数
            MultiValueMap<String, String> params = exchange.getRequest().getQueryParams();
            List<String> values = params.get("token");
            return values.contains(config.getToken());
        };
    }

    @Data
    public static class Config {
        private String token;
    }

    @Override
    public List<String> shortcutFieldOrder() {
        return Collections.singletonList("token");
    }
}
