package com.abc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiGateway9001 {

    public static void main(String[] args) {
        SpringApplication.run(ApiGateway9001.class, args);
    }

}
