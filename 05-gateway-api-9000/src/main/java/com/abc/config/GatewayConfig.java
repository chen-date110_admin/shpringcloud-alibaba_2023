package com.abc.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayConfig {

    @Bean
    public RouteLocator filterrRewritePathRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("filterrRewritePath_route",
                        ps -> ps.path("/red/**")
                                .filters(fs -> fs.rewritePath("/red/?(?<segment>.*)", "/$\\{segment}"))
                                .uri("https://localhost:8081"))
                .build();
    }
    @Bean
    public RouteLocator filterrStripPrefixRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("filterrStripPrefix_route",
                        ps -> ps.path("/*/*/provider/depart/**")
                                .filters(fs -> fs.stripPrefix(2))
                                .uri("https://localhost:8081"))
                .build();
    }
    @Bean
    public RouteLocator filterrPrefixPathRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("filterrPrefixPath_route",
                        ps -> ps.path("/depart/**")
                                .filters(fs -> fs.prefixPath("/provider"))
                                .uri("https://localhost:8081"))
                .build();
    }
    @Bean
    public RouteLocator filterCircuitBreakerRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("filterCircuitBreaker_route",
                        ps -> ps.path("/**")
                                .filters(fs -> fs.circuitBreaker(config -> {
                                    config.setName("myCircuitBreaker");
                                    config.setFallbackUri("forward:/fb");
                                }))
                                .uri("https://localhost:8080"))
                .build();
    }
    @Bean
    public RouteLocator filterAddResponseHeaderRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("filterAddResponseHeader_route",
                        ps -> ps.path("/**")
                                .filters(fs -> fs.addResponseHeader("color", "blue")
                                        .addResponseHeader("color", "red")
                                        .addResponseHeader("city", "beijing")
                                        .addResponseHeader("fruit", "apple"))
                                .uri("https://localhost:8080"))
                .build();
    }
    public RouteLocator filterAddRequestParameterRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("filterAddRequestParameter_route",
                        ps -> ps.path("/**")
                                .filters(fs -> fs.addRequestParameter("color", "blue")
                                        .addRequestParameter("color", "red")
                                        .addRequestParameter("city", "beijing")
                                        .addRequestParameter("fruit", "apple"))
                                .uri("https://localhost:8080"))
                .build();
    }
    @Bean
    public RouteLocator filterAddRequestHeadersIfNotPresentRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("filterAddRequestHeadersIfNotPresent_route",
                        ps -> ps.path("/**")
                                .filters(fs -> fs.addRequestHeadersIfNotPresent("X-Request-Color:blue", "city:beijing")
                                                 .addRequestHeadersIfNotPresent("X-Request-Color:red", "school:tsinghua"))
                                .uri("https://localhost:8080"))
                .build();
    }
    @Bean
    public RouteLocator filterAddRequestHeaderRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("filterAddRequestHeader_route",
                        ps -> ps.path("/**")
                                .filters(fs -> fs.addRequestHeader("X-Request-Color", "blue")
                                                 .addRequestHeader("X-Request-Color", "red")
                                                 .addRequestHeader("X-Request-Color", "green"))
                                .uri("https://localhost:8080"))
                .build();
    }

    @Bean
    public RouteLocator weightRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("weight_route",
                        ps -> ps.weight("provider", 2)
                                .uri("https://localhost:8081"))
                .route("weight_route2",
                        ps -> ps.weight("provider", 3)
                                .uri("http://localhost:8082"))
                .build();
    }
    @Bean
    public RouteLocator queryRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("query_route",
                        ps -> ps.query("coror", "gr.+")
                                .and()
                                .query("size")
                                .uri("http://localhost:8081"))
                .build();
    }
    @Bean
    public RouteLocator pathRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("path_route",
                        ps -> ps.path("/provider/**")
                                .uri("http://localhost:8081"))
                .route("path_route2",
                        ps -> ps.path("/consumer/**")
                                .uri("http://localhost:8080"))
                .build();
    }
    @Bean
    public RouteLocator methodRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("method_route",
                        ps -> ps.method("GET", "POST")
                                .uri("http://localhost:8081"))
                .build();
    }
    @Bean
    public RouteLocator hostRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("host_route",
                        ps -> ps.host("aaa.com", "bbb.com")
                                .uri("http://localhost:8081"))
                .build();
    }
    @Bean
    public RouteLocator headerRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("header_route",
                        ps -> ps.header("X-Request-Id", "abc")
                                .uri("http://localhost:8081"))
                .build();
    }
}
