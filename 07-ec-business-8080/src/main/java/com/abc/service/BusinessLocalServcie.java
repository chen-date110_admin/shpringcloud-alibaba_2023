package com.abc.service;

import org.apache.skywalking.apm.toolkit.trace.Trace;

public class BusinessLocalServcie {

    @Trace
    public void info() {
        System.out.println("Auth:" + auth("Tom") + "，email:" + email("Tom@163.com"));
    }

    @Trace
    private String email(String addr) {
        return addr;
    }

    private String auth(String name) {
        return name;
    }
}
