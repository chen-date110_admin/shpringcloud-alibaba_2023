package com.abc.parser;

import com.alibaba.csp.sentinel.adapter.spring.webmvc.callback.RequestOriginParser;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class DepartRequestOriginParser implements RequestOriginParser {
    // 假设请求源标识是以请求参数的形式出现
    // Sentinel会自动根据parserOrigin()方法的返回结果来判断请求源
    @Override
    public String parseOrigin(HttpServletRequest request) {
        // 获取请求参数source的值，即请求源标识
        String source = request.getParameter("source");
        // 若请求中没有携带source请求参数，则默认请求源为sa
        if (!StringUtils.hasText(source)) {
            source = "sa";
        }
        return source;
    }
}
