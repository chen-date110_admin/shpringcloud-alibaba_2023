package com.abc;

import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRuleManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class SentinelConsumer8080 {

    public static void main(String[] args) {
        SpringApplication.run(SentinelConsumer8080.class, args);
        // initFlowRule();
    }

    private static void initFlowRule() {
        List<FlowRule> list = new ArrayList<>();
        FlowRule rule = SentinelConsumer8080.configFlowRule();
        list.add(rule);
        FlowRuleManager.loadRules(list);
    }

    private static FlowRule configFlowRule() {
        FlowRule rule = new FlowRule();
        rule.setResource("getHandle");
        // 指定规则类型为QPS流控规则
        rule.setGrade(RuleConstant.FLOW_GRADE_QPS);
        rule.setStrategy(RuleConstant.STRATEGY_CHAIN);
        rule.setRefResource("/provider/depart/all");

        rule.setControlBehavior(RuleConstant.CONTROL_BEHAVIOR_WARM_UP_RATE_LIMITER);
        rule.setWarmUpPeriodSec(5);
        rule.setMaxQueueingTimeMs(20*1000);

        // 指定QPS阈值
        rule.setCount(5);
        // 指定请求来源
        rule.setLimitApp("default");
        return rule;
    }

}
