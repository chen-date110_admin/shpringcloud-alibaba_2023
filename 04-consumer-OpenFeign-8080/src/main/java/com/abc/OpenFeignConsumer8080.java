package com.abc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class OpenFeignConsumer8080 {
    public static void main(String[] args) {
        SpringApplication.run(OpenFeignConsumer8080.class, args);
    }
}
