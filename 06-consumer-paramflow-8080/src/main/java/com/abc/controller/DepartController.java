package com.abc.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DepartController {


    @SentinelResource(value = "paramHandle", fallback = "paramHandleFallback")
    @GetMapping("/param")
    public String paramHandle(Integer id, String name) {
        return "param flow：" + id + "，" + name;
    }

    public String paramHandleFallback(Integer id, String name) {
        return "fallback：" + id + "，" + name;
    }

}
