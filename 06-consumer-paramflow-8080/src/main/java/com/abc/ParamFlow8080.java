package com.abc;

import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowItem;
import com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowRuleManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class ParamFlow8080 {

    public static void main(String[] args) {
        SpringApplication.run(ParamFlow8080.class, args);
        initRule();
    }

    private static void initRule() {
        List<ParamFlowRule> list = new ArrayList<>();
        ParamFlowRule rule = ParamFlow8080.configRule();
        list.add(rule);
        ParamFlowRuleManager.loadRules(list);
    }

    private static ParamFlowRule configRule() {
        ParamFlowRule rule = new ParamFlowRule();
        rule.setResource("paramHandle");
        // 必须要指定规则类型为QPS流控
        rule.setGrade(RuleConstant.FLOW_GRADE_QPS);
        rule.setCount(3);
        // 指定对索引为1的参数进行流控
        rule.setParamIdx(1);
        // 指定熔断窗口时长为10秒
        rule.setDurationInSec(10);

        List<ParamFlowItem> items = new ArrayList<>();
        items.add(nameParamItem("admin", 150));
        items.add(nameParamItem("administrator", 150));
        items.add(nameParamItem("zs", 250));
        rule.setParamFlowItemList(items);
        return rule;
    }

    private static ParamFlowItem nameParamItem(String name, int count) {
        ParamFlowItem item = new ParamFlowItem();
        // 指定热点参数类型
        item.setClassType(String.class.getName());
        // 指定热点参数例外值
        item.setObject(String.valueOf(name));
        // 指定热点参数例外项的特殊QPS阈值
        item.setCount(count);
        return item;
    }

}
