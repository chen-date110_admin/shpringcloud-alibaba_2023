package com.abc.controller;

import com.abc.bean.Account;
import com.abc.bean.Goodsorder;
import com.abc.service.AccountService;
import com.abc.service.GoodsorderServiceLocal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GoodsorderController {

    // 注意，这里的要保证与ec-common工程中的GoodsorderService Feign接口名称不相同，
    // 否则无法启动。因为系统不知道是调用本地的GoodsorderService还是Feign接口的
    @Autowired
    private GoodsorderServiceLocal goodsorderService;

    @Autowired
    private AccountService accountService;

    @PostMapping("/goodsorder/create")
    public Goodsorder createHandle(@RequestBody Goodsorder goodsorder) {
        // 根据userId查询其账户的余额，判断是否足够满足本次购买
        // 计算总消费额
        double total = goodsorder.getGoodsPrice() * goodsorder.getCount();
        Account account = accountService.debitAccount(goodsorder.getUserId(), total);
        // 账户余额不足，则返回null
        if (account == null) {
            return null;
        }

        // int i = 3/0;

        // 在账户余额充足的情况下，在订单表中生成一条订单记录
        Goodsorder order = goodsorderService.createGoodsorder(goodsorder);

        // int i = 3/0;

        return order;
    }

}
