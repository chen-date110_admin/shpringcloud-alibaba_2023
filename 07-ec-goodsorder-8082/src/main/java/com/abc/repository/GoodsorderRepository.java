package com.abc.repository;

import com.abc.bean.Goodsorder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GoodsorderRepository extends JpaRepository<Goodsorder, Integer> {
}
