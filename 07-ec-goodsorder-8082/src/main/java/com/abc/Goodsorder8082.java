package com.abc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class Goodsorder8082 {

    public static void main(String[] args) {
        SpringApplication.run(Goodsorder8082.class, args);
    }

}
