package com.abc.service;

import com.abc.bean.Goodsorder;
import com.abc.repository.GoodsorderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GoodsorderServiceImpl implements GoodsorderServiceLocal {

    @Autowired
    private GoodsorderRepository repository;

    @Override
    public Goodsorder createGoodsorder(Goodsorder goodsorder) {
        return repository.save(goodsorder);
    }
}
