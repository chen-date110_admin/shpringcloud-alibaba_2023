package com.abc.controller;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Enumeration;
import java.util.Map;

@RestController
@RequestMapping("/info/")
public class ShowInfoController {

    @GetMapping("/time")
    public String timeHandle() {
        return "到达目标服务器的时间：" + System.currentTimeMillis();
    }
    @GetMapping("/param")
    public String paramHandle(String[] color, int size, String fruit) {
        StringBuilder sb = new StringBuilder();
        sb.append("color:");
        for (String c : color) {
            sb.append(c + " ");
        }
        sb.append("<br>size:" + size);
        sb.append("<br>fruit:" + fruit);
        return sb.toString();
    }
    @GetMapping("/params")
    public String paramsHandle(HttpServletRequest request) {
        Map<String, String[]> map = request.getParameterMap();
        StringBuilder sb = new StringBuilder();
        for (String key : map.keySet()) {
            sb.append(key + ":");
            for (String value : map.get(key)) {
                sb.append(value + " ");
            }
            sb.append("<br>");
        }
        return sb.toString();
    }
    @GetMapping("/headers")
    public String headersHandle(HttpServletRequest request) {
        // 获取所有header的key
        Enumeration<String> headerNames = request.getHeaderNames();
        StringBuilder sb = new StringBuilder();
        while (headerNames.hasMoreElements()) {
            String name = headerNames.nextElement();
            sb.append(name + ":");
            // 获取当前遍历headerName的所有value
            Enumeration<String> headers = request.getHeaders(name);
            while (headers.hasMoreElements()) {
                sb.append(headers.nextElement() + " ");
            }
            sb.append("<br>");
        }
        return sb.toString();
    }
    @GetMapping("/header")
    public String headerHandle(HttpServletRequest request) {
        Enumeration<String> headers = request.getHeaders("X-Request-Color");
        StringBuilder sb = new StringBuilder();
        while (headers.hasMoreElements()) {
            sb.append(headers.nextElement() + " ");
        }
        return "X-Request-Color：" + sb.toString();
    }
}
