package com.abc.factory;

import lombok.Data;
import org.springframework.cloud.gateway.handler.predicate.AbstractRoutePredicateFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

@Component
public class AuthRoutePredicateFactory extends AbstractRoutePredicateFactory<AuthRoutePredicateFactory.Config> {

    public AuthRoutePredicateFactory() {
        super(Config.class);
    }

    @Override
    public Predicate<ServerWebExchange> apply(Config config) {
        return exchange -> {
            // 获取到请求中的所有header
            HttpHeaders headers = exchange.getRequest().getHeaders();
            // 一个请求头可以包含多个值
            List<String> pwds = headers.get(config.getUsername());
            // 只要请求头中指定的多个密码值中包含了配置文件中指定的密码，就可以通过
            String[] values = pwds.get(0).split(",");

            for (String value : values) {
                if (value.equalsIgnoreCase(config.getPassword())) {
                    return true;
                }
            }

            return false;
        };
    }

    @Data
    public static class Config {
        private String username;
        private String password;
    }

    @Override
    public List<String> shortcutFieldOrder() {
        return Arrays.asList("username", "password");
    }
}
