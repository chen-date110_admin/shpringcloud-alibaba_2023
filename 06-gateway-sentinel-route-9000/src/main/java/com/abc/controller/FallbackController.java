package com.abc.controller;

import org.springframework.web.bind.annotation.RequestMapping;

// @RestController
public class FallbackController {

    @RequestMapping("/fb")
    public String fallbackHandle() {
        return "this is the Gateway Fallback.";
    }

    @RequestMapping("/fb2")
    public String fallbackHandle2() {
        return "this is the Gateway Fallback2222222222.";
    }

    @RequestMapping("/fb3")
    public String fallbackHandle3() {
        return "this is the Gateway Fallback3333333333333.";
    }
}
