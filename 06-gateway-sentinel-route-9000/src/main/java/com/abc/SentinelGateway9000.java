package com.abc;

import com.alibaba.csp.sentinel.adapter.gateway.common.SentinelGatewayConstants;
import com.alibaba.csp.sentinel.adapter.gateway.common.rule.GatewayFlowRule;
import com.alibaba.csp.sentinel.adapter.gateway.common.rule.GatewayParamFlowItem;
import com.alibaba.csp.sentinel.adapter.gateway.common.rule.GatewayRuleManager;
import com.alibaba.csp.sentinel.adapter.gateway.sc.callback.GatewayCallbackManager;
import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerResponse;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@SpringBootApplication
public class SentinelGateway9000 {

    public static void main(String[] args) {
        SpringApplication.run(SentinelGateway9000.class, args);
        // initBlockHandler();
        // initGatewayRule();
    }

    private static void initGatewayRule() {
        Set<GatewayFlowRule> rules = new HashSet<>();
        GatewayFlowRule rule = SentinelGateway9000.configGatewayFlowRule();
        rules.add(rule);
        GatewayRuleManager.loadRules(rules);
    }

    private static GatewayFlowRule configGatewayFlowRule() {
        GatewayFlowRule rule = new GatewayFlowRule();
        // 指定资源模式为route，默认值
        rule.setResourceMode(SentinelGatewayConstants.RESOURCE_MODE_ROUTE_ID);
        // 指定要限流的route的id
        rule.setResource("get_route");
        // 指定限流规则为QPS流控，默认值
        rule.setGrade(RuleConstant.FLOW_GRADE_QPS);
        // 指定QPS阈值
        rule.setCount(3);
        // 指定统计时长，默认值为1秒
        rule.setIntervalSec(1);
        // 指定流控方式为“快速失败”，默认值
        // rule.setControlBehavior(RuleConstant.CONTROL_BEHAVIOR_DEFAULT);
        // 指定流控方式为“匀速排队"
        rule.setControlBehavior(RuleConstant.CONTROL_BEHAVIOR_RATE_LIMITER);
        // 指定排队等待的超时时间，默认500毫秒
        rule.setMaxQueueingTimeoutMs(500);

        GatewayParamFlowItem item = new GatewayParamFlowItem();
        // 指定针对的”参数属性“类型为”URL参数“
        item.setParseStrategy(SentinelGatewayConstants.PARAM_PARSE_STRATEGY_URL_PARAM);
        // 指定”URL参数名称“
        item.setFieldName("name");
        // 指定匹配模式为”子串“
        item.setMatchStrategy(SentinelGatewayConstants.PARAM_MATCH_STRATEGY_CONTAINS);
        item.setPattern("ang");

        rule.setParamItem(item);

        return rule;
    }

    private static void initBlockHandler() {
        // 重定向
        // GatewayCallbackManager.setBlockHandler(new RedirectBlockRequestHandler("https://baidu.com"));

        // 自定义异常结果
        GatewayCallbackManager.setBlockHandler((exchange,th) -> {
            Map<String, Object> map = new HashMap<>();
            map.put("uri", exchange.getRequest().getURI());
            map.put("msg", "访问量过大，稍候请重试");
            map.put("status", 429);
            return ServerResponse.status(HttpStatus.TOO_MANY_REQUESTS)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(BodyInserters.fromValue(map));
        });

    }

}
