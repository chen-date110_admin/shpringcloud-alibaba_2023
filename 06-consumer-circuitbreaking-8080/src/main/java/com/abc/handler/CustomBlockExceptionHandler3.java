package com.abc.handler;

import com.alibaba.csp.sentinel.adapter.spring.webmvc.callback.BlockExceptionHandler;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.authority.AuthorityException;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeException;
import com.alibaba.csp.sentinel.slots.block.flow.FlowException;
import com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowException;
import com.alibaba.csp.sentinel.slots.system.SystemBlockException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

// @Component
public class CustomBlockExceptionHandler3 implements BlockExceptionHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, BlockException e) throws Exception {

        String page = "";

        if (e instanceof FlowException) {
            page = "/flow.html";
        } else if(e instanceof DegradeException) {
            page = "/degrade.html";
        } else if(e instanceof SystemBlockException) {
            page = "/system.html";
        } else if(e instanceof ParamFlowException) {
            page = "/param.html";
        } else if(e instanceof AuthorityException) {
            page = "/authority.html";
        }

        request.getRequestDispatcher(page).forward(request, response); //重定向
    }
}
