package com.abc.handler;

import com.alibaba.csp.sentinel.adapter.spring.webmvc.callback.BlockExceptionHandler;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.authority.AuthorityException;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeException;
import com.alibaba.csp.sentinel.slots.block.flow.FlowException;
import com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowException;
import com.alibaba.csp.sentinel.slots.system.SystemBlockException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.PrintWriter;

// @Component
public class CustomBlockExceptionHandler2 implements BlockExceptionHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, BlockException e) throws Exception {
        response.setStatus(429);

        PrintWriter out = response.getWriter();
        String msg = "Blocked by Sentinel - ";

        if (e instanceof FlowException) {
            msg += "Flow Exception";
        } else if(e instanceof DegradeException) {
            msg += "Degrade Exception";
        } else if(e instanceof SystemBlockException) {
            msg += "System Block Exception";
        } else if(e instanceof ParamFlowException) {
            msg += "Param Flow Exception";
        } else if(e instanceof AuthorityException) {
            msg += "Authority Exception";
            response.setStatus(401);
        }

        out.print(msg);
        out.flush();
        out.close();
    }
}
