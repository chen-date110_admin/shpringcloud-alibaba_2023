package com.abc;

import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRule;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRuleManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class SentinelConsumer8080 {

    public static void main(String[] args) {
        SpringApplication.run(SentinelConsumer8080.class, args);
        initDegradeRule();
    }

    private static void initDegradeRule() {
        List<DegradeRule> rules = new ArrayList<>();
        DegradeRule rule = SentinelConsumer8080.configSlowDegradeRule();
        rules.add(rule);
        DegradeRuleManager.loadRules(rules);
    }

    // 设置慢调用比例熔断规则
    private static DegradeRule configSlowDegradeRule() {
        DegradeRule rule = new DegradeRule();
        // 指定将当前规则应用于的Sentinel资源名称
        rule.setResource("getHandle");
        // 指定熔断策略为慢调用比例
        rule.setGrade(RuleConstant.DEGRADE_GRADE_RT);
        // 指定最大RT
        rule.setCount(2);
        // 指定比例阈值
        rule.setSlowRatioThreshold(0.2);
        // 指定熔断时长
        rule.setTimeWindow(10);
        // 指定统计时长
        rule.setStatIntervalMs(1000);
        // 指定最小请求数
        rule.setMinRequestAmount(2);
        return rule;
    }

    // 设置异常比例熔断规则
    private static DegradeRule configErrorRatioDegradeRule() {
        DegradeRule rule = new DegradeRule();
        // 指定将当前规则应用于的Sentinel资源名称
        rule.setResource("getHandle");
        // 指定熔断策略为异常比例
        rule.setGrade(RuleConstant.DEGRADE_GRADE_EXCEPTION_RATIO);
        // 指定比例阈值
        rule.setCount(0.3);
        // 指定熔断时长
        rule.setTimeWindow(10);
        // 指定统计时长
        rule.setStatIntervalMs(1000);
        // 指定最小请求数
        rule.setMinRequestAmount(2);
        return rule;
    }

    // 设置异常数量熔断规则
    private static DegradeRule configErrorCountDegradeRule() {
        DegradeRule rule = new DegradeRule();
        // 指定将当前规则应用于的Sentinel资源名称
        rule.setResource("getHandle");
        // 指定熔断策略为异常数量
        rule.setGrade(RuleConstant.DEGRADE_GRADE_EXCEPTION_COUNT);
        // 指定比例阈值
        rule.setCount(30);
        // 指定熔断时长
        rule.setTimeWindow(10);
        // 指定统计时长
        rule.setStatIntervalMs(1000);
        // 指定最小请求数
        rule.setMinRequestAmount(2);
        return rule;
    }
}
