package com.abc.controller;

import com.abc.bean.Account;
import com.abc.service.AccountService;
import org.apache.skywalking.apm.toolkit.trace.TraceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class AccountController {

    @Autowired
    private AccountService service;

    @PutMapping("/account/debit")
    public Account debitHandle(@RequestParam("userId") Integer userId,
                                @RequestParam("amount") Double amount) {

        String traceId = TraceContext.traceId();
        System.out.println("traceId ======= " + traceId);

        Optional<String> businessTime = TraceContext.getCorrelation("businessTime");
        System.out.println("businessTime ======= " + businessTime.get());

        return service.debitAccount(userId, amount);
    }

}
