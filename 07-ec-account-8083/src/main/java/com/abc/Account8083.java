package com.abc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Account8083 {

    public static void main(String[] args) {
        SpringApplication.run(Account8083.class, args);
    }

}
