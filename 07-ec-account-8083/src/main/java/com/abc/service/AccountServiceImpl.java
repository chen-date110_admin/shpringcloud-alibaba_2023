package com.abc.service;

import com.abc.bean.Account;
import com.abc.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository repository;

    @Override
    public Account debitAccount(Integer userId, Double amount) {
        Account account = repository.findAccountByuserId(userId);
        if (account == null || account.getBalance() < amount) {
            return null;
        }
        account.setBalance(account.getBalance() - amount);
        return repository.save(account);
    }
}
