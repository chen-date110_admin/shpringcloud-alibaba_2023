package com.abc.service;

import com.abc.bean.Account;

public interface AccountService {
    Account debitAccount(Integer userId, Double amount);
}
