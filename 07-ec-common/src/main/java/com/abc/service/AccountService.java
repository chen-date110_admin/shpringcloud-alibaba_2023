package com.abc.service;

import com.abc.bean.Account;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("ec-account")
public interface AccountService {
    @PutMapping("/account/debit")
    Account debitAccount(@RequestParam("userId") Integer userId,
                         @RequestParam("amount") Double amount);
}
