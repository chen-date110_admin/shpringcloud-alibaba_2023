package com.abc.service;

import com.abc.bean.Goodsorder;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("ec-goodsorder")
public interface GoodsorderService {
    @PostMapping("/goodsorder/create")
    Goodsorder createGoodsorder(@RequestBody Goodsorder goodsorder);
}
