package com.abc.service;

import com.abc.bean.Stock;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("ec-stock")
public interface StockService {
    @PutMapping("/stock/deduct")
    Stock deductStock(@RequestParam("goodsId") Integer goodsId,
                      @RequestParam("count") Integer count);
}
