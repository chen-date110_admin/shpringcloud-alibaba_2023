package com.abc.bean;

import lombok.Data;

@Data
public class Stock {
    private Integer id;
    private Integer goodsId;  // 商品id
    private Integer total;  // 库存量
}
