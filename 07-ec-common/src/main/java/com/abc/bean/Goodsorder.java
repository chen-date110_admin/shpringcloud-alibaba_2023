package com.abc.bean;

import lombok.Data;

@Data
public class Goodsorder {
    private Integer id;
    private Integer userId;  // 用户id
    private Integer goodsId; // 商品id
    private Double goodsPrice; // 商品单价
    private Integer count; // 购买量
}
