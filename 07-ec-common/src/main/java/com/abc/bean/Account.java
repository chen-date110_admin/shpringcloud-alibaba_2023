package com.abc.bean;

import lombok.Data;

@Data
public class Account {
    private Integer id;
    private Integer userId;  // 用户id
    private Double balance;  // 账户余额
}
