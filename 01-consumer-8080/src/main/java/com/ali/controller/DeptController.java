package com.ali.controller;

import com.ali.admin.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("/consumer/dept")
public class DeptController {
    @Autowired
    private RestTemplate template;
    private static final String SERVER_PROCIER = "http://localhost:8081/dept/";

    @PostMapping("/save")
    public boolean saveHandle(@RequestBody Dept dept) {
        String url=SERVER_PROCIER+"save";
        return Boolean.TRUE.equals(template.postForObject(url, dept, Boolean.class));
    }

    @DeleteMapping("/delete/{id}")
    public void deleteHandle(@PathVariable int id) {
        String url=SERVER_PROCIER+"delete/";
        template.delete(url + id);
    }
    @PutMapping("/update")
    public void updateHandle(@RequestBody Dept dept){
        String url=SERVER_PROCIER+"update/";
        template.put(url,dept);
    }
    @GetMapping("/get/{id}")
    public Dept getHandle( @PathVariable int id){
        String url=SERVER_PROCIER+"get/"+id;
       return template.getForObject(url,Dept.class);
    }
    @GetMapping("/list")
    public List<Dept>getAll(){
        String url=SERVER_PROCIER+"list";
        return template.getForObject("http://localhost:8081/dept/", List.class);
    }
}
