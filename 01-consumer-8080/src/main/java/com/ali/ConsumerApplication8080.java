package com.ali;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumerApplication8080 {

	public static void main(String[] args) {
		SpringApplication.run(ConsumerApplication8080.class, args);
	}

}
