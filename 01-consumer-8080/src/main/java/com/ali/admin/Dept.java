package com.ali.admin;

import lombok.Data;

@Data
public class Dept {
    private Integer id;
    private String name;
}
