package com.abc.service;

import com.abc.bean.Goodsorder;
import com.abc.repository.GoodsorderRepository;
import io.seata.rm.tcc.api.BusinessActionContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class GoodsorderServiceImpl implements GoodsorderServiceLocal {

    @Autowired
    private GoodsorderRepository repository;
    private Integer goodsorderId;

    @Override
    public Goodsorder createGoodsorder(Goodsorder goodsorder) {
        Goodsorder order = repository.save(goodsorder);
        this.goodsorderId = order.getId();
        return order;
    }

    @Override
    public Boolean tccCommit(BusinessActionContext context) {
        log.info("订单表记录插入成功");
        return true;
    }

    @Override
    public Boolean tccRollback(BusinessActionContext context) {
        log.info("订单表发生回滚");
        repository.deleteById(goodsorderId);
        return true;
    }
}
