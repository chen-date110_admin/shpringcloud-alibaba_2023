package com.abc.service;

import com.abc.bean.Goodsorder;
import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.BusinessActionContextParameter;
import io.seata.rm.tcc.api.LocalTCC;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;

@LocalTCC
public interface GoodsorderServiceLocal {

    @TwoPhaseBusinessAction(name = "createGoodsorder",
            commitMethod = "tccCommit",
            rollbackMethod = "tccRollback")
    Goodsorder createGoodsorder(@BusinessActionContextParameter("goodsorder") Goodsorder goodsorder);

    // 手动提交方法
    Boolean tccCommit(BusinessActionContext context);

    // 手动回滚方法
    Boolean tccRollback(BusinessActionContext context);
}
