package com.abc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class TccGoodsorder8082 {

    public static void main(String[] args) {
        SpringApplication.run(TccGoodsorder8082.class, args);
    }

}
